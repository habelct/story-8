from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve
from . import views

# Create your tests here.
class TestStatusPage(TestCase):
    def test_url_is_exist(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)

    def test_using_homepage_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'homepage.html')
    
    def test_func(self):
        found = resolve('/') 
        self.assertEqual(found.func, views.homepage)
    
    def test_konten_apakah_terdapat_judul(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Search any Books!", content)


